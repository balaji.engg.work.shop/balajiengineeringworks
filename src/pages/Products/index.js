import { useNavigate } from "react-router-dom";
import { productList } from "../../data";

export function Products() {
  const navigate = useNavigate();

  return (
    <div className="px-2 lg:px-20 py-2 lg:py-10">
      <div className="grid grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-5 lg:gap-20">
        {productList?.map((data, index) => (
          <div
            key={index}
            className="bg-teal-800 rounded-2xl p-2 lg:p-4 cursor-pointer hover:scale-105 transition-transform"
            onClick={() =>
              navigate(`/products/${data.name}`, {
                state: {
                  data: data.products,
                  name: data.name,
                },
              })
            }
          >
            <img
              src={data.image}
              alt={data.name}
              className="w-full h-40 lg:h-80 rounded-2xl object-cover"
              loading="lazy"
            />
            <div className="pt-1 lg:pt-3">
              <p className="font-semibold text-center py-1 lg:py-3 text-white rounded-full hover:bg-orange-500 transition-colors">
                {data.name}
              </p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
