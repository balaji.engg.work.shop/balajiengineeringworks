export { HomePage } from "./HomePage";
export { ContactUs } from "./ContactUs";
export { Products } from "./Products";
export { ShopDetails } from "./ShopDetails";
export { SingleProductType } from "./SingleProductType";
// export { MyMap } from "./MyMap";
export { Gallery } from "./Gallery";
