import { HiOutlineArrowNarrowRight } from "react-icons/hi";
import { useNavigate } from "react-router-dom";
import SimpleImageSlider from "react-simple-image-slider";
import { gate, pipes } from "../../accets";
import { cofounder, productImagesURL } from "../../data";

const ourOffering = [
  { image: pipes, url: "/products", label: "Stainless Steel Items" },
  { image: gate, url: "/products", label: "Iron Gates and Grills" },
  { image: "", url: "/products", label: "Glass Door & Window" },
];

export function HomePage() {
  const navigate = useNavigate();
  return (
    <div>
      <h1 className="text-3xl font-bold underline text-center">Hello World!</h1>
      <div className="hidden md:block">
        <SimpleImageSlider
          width="100%"
          height="900px"
          images={productImagesURL}
          showBullets
          showNavs
          autoPlay
        />
      </div>
      <div className="md:hidden">
        <SimpleImageSlider
          width="100%"
          height="250px"
          images={productImagesURL}
          showBullets
          showNavs
          autoPlay
        />
      </div>
      <div className="bg-gray-800 py-10 lg:py-20 text-white text-center">
        <h2 className="text-3xl lg:text-5xl font-bold">Who We Are</h2>
        <p className="text-lg lg:text-xl mt-4 px-5 lg:px-52">
          We are Balajee Group, one of the largest and best-managed
          organizations in Ara, Bihar. Established in 1998, we specialize in
          manufacturing iron gates, windows, steel balusters, and pipes.
        </p>
        <button className="text-orange-500 text-lg flex items-center gap-2 mt-5">
          KNOW MORE <HiOutlineArrowNarrowRight size={20} />
        </button>
      </div>
      <div className="text-center py-10">
        <h2 className="text-4xl font-bold text-gray-800">Our Offerings</h2>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 px-5 lg:px-20 mt-5">
          {ourOffering.map((data, index) => (
            <div
              key={index}
              className="p-5 border rounded-lg shadow-lg cursor-pointer"
              onClick={() => navigate(data.url)}
            >
              <h3 className="text-xl font-bold text-orange-500">
                {data.label}
              </h3>
              <button className="text-orange-500 flex items-center gap-2 mt-2">
                KNOW MORE <HiOutlineArrowNarrowRight size={20} />
              </button>
              <img
                src={data.image}
                alt={data.label}
                className="w-full h-64 object-cover mt-4"
              />
            </div>
          ))}
        </div>
      </div>
      <div className="bg-orange-500 text-white text-center py-10">
        <h2 className="text-4xl font-bold text-black">Our Diverse Portfolio</h2>
        <p className="text-lg lg:text-xl mt-4 px-5 lg:px-52 text-black">
          We manufacture SS pipes, balusters, master pillars, iron gates, steel
          gates, railings, glass doors, sliding windows, and much more.
        </p>
      </div>
      <div className="py-10 text-center">
        <h2 className="text-4xl font-bold">Co-founders</h2>
        <div className="grid grid-cols-2 gap-6 mt-5 justify-center">
          {cofounder.map((data, index) => (
            <div key={index} className="text-center">
              <img
                src={data.image}
                alt={data.name}
                className="w-32 h-32 rounded-full mx-auto"
              />
              <h3 className="text-xl font-bold mt-3">{data.name}</h3>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
