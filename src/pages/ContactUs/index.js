import { shopList } from "../../data";
import { BalajeeMap } from "../../accets";
import { IoMdCall } from "react-icons/io";
import { PiMapPinLineFill } from "react-icons/pi";
import { AiOutlineUser, AiTwotoneShop } from "react-icons/ai";
import { BsClockHistory } from "react-icons/bs";

export function ContactUs() {
  return (
    <div>
      <div
        className="bg-cover bg-center h-[200px] lg:h-[800px] w-full"
        style={{
          backgroundImage:
            "url(https://ipfs.io/ipfs/bafkreicldnx5kopdisqu64azcqrjzyffro3xzrgx5ra547dvzhxkpzf7ha)",
        }}
      >
        <h1 className="text-white text-center pt-20 lg:pt-[400px] text-4xl lg:text-6xl font-semibold">
          Contact Us
        </h1>
      </div>
      <div className="bg-[#f58634] text-[#2A2A2A] py-3 lg:py-10 px-5">
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-5 lg:gap-40">
          <div>
            <h2 className="text-lg lg:text-3xl font-semibold">
              MANUFACTURING UNITS :
            </h2>
            <p className="text-md lg:text-xl font-normal lg:w-[200px]">
              Tetaria more, Ekawana, Ara Bhojpur, Bihar - 802301
            </p>
            <h2 className="text-lg lg:text-3xl font-semibold pt-3 lg:pt-10">
              PHONE :
            </h2>
            <p className="text-md lg:text-xl font-normal">
              Tel : +919650255062 , +918252285896
            </p>
            <h2 className="text-lg lg:text-3xl font-semibold pt-1 lg:pt-3">
              EMAIL :
            </h2>
            <p className="text-md lg:text-xl font-normal">
              Vishalkumar70522@gmail.com
            </p>
          </div>
          <div>
            <h2 className="text-lg lg:text-3xl font-semibold">
              REGISTERED OFFICE :
            </h2>
            <p className="text-md lg:text-xl font-normal lg:w-[200px]">
              Balajee Steel Shop, Near Bus Stand, Ara Bhojpur, Bihar - 802301
            </p>
            <h2 className="text-lg lg:text-3xl font-semibold pt-3 lg:pt-10">
              PHONE :
            </h2>
            <p className="text-md lg:text-xl font-normal">
              Tel : +919650255062 , +919939397519
            </p>
            <h2 className="text-lg lg:text-3xl font-semibold pt-1 lg:pt-3">
              EMAIL :
            </h2>
            <p className="text-md lg:text-xl font-normal">
              Vishalkumar70522@gmail.com
            </p>
          </div>
        </div>
      </div>
      <div
        className="cursor-pointer"
        onClick={() =>
          window.open(
            "https://www.google.com/maps/place/Balajee+engineering+works/@25.5209514,84.6346561,16.31z/data=!4m6!3m5!1s0x398d5f800d163033:0x2e56a84df5854253!8m2!3d25.5199473!4d84.6395915!16s%2Fg%2F11kgp4bsv2?entry=ttu",
            "_blank"
          )
        }
      >
        <img
          src={BalajeeMap}
          alt="Google Map"
          className="h-[200px] lg:h-[500px] w-full"
        />
      </div>
      <div className="p-2 lg:p-10">
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-1 lg:gap-10">
          {shopList?.map((shop, index) => (
            <div
              key={index}
              className="p-5 border border-gray-300 hover:shadow-2xl transition-shadow"
            >
              <div className="flex items-center gap-2">
                <AiTwotoneShop className="text-[#f58634]" />
                <div className="flex gap-5">
                  <p>{shop.shopName}</p>
                  <p className="text-[#900C3F]">{shop.shopType}</p>
                </div>
              </div>
              <div className="flex items-center gap-2">
                <PiMapPinLineFill className="text-[#f58634]" />
                <p>{shop.fullAddress}</p>
              </div>
              <div className="flex items-center gap-2 pt-2">
                <AiOutlineUser className="text-[#f58634]" />
                <p>{shop.OwnerName}</p>
              </div>
              <div className="flex items-center gap-2 pt-2">
                <IoMdCall className="text-[#f58634]" />
                <p>{shop.tel}</p>
              </div>
              <div className="flex items-center gap-2 pt-2">
                <BsClockHistory className="text-[#f58634]" />
                <p>{shop.open}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
