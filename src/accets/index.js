export { default as Vishal } from "./images/vishal.jpeg";
export { default as logo } from "./images/logo.jpg";
export { default as BalajeeMap } from "./images/BalajeeMap.png";
export { default as laldevPd } from "./images/laldevPd.jpeg";
export { default as baldevPd } from "./images/baldevpd.jpg";
export { default as madankr } from "./images/madanpd.jpg";
export { default as gate } from "./images/gate.jpg";
export { default as masterpiler } from "./images/masterpiler.jpg";
export { default as pipes } from "./images/pipes.jpg";
export { default as dashrath } from "./images/dashrath.jpeg";
export { default as upendra } from "./images/upendra.jpeg";
export { default as anil } from "./images/anil.jpeg";

export { default as visitingCard } from "./images/visitingCard.jpeg";

//gallery
// export { default as g1 } from "./gallery/g-11.jpg";
// export { default as g2 } from "./gallery/g-12.jpg";
// export { default as g3 } from "./gallery/g-13.jpg";
// export { default as g4 } from "./gallery/g-17.jpg";
// export { default as g5 } from "./gallery/g-19.jpg";
// export { default as g6 } from "./gallery/g-24.jpg";
// export { default as g8 } from "./gallery/g-25.jpg";
// export { default as g9 } from "./gallery/g-3.jpg";
// export { default as g10 } from "./gallery/g-34.jpg";
// export { default as g11 } from "./gallery/g-37.jpg";
// export { default as g12 } from "./gallery/g-7.jpg";
// export { default as g13 } from "./gallery/g-8.jpg";
